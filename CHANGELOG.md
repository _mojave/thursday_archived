# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.0.33] - 2017-05-15
### Changed
- Updated Settings, user models so that users can add a bio and location. Also gave some vertical margin between user cards in the Users route.

## [0.0.32] - 2017-05-14
### Changed
- Updated Register route and component to handle app sign-ups.

### Added
- Registration Guard to prevent accendental exposure to the registration route if navigate directly there while it's disabled

## [0.0.31] - 2017-05-13
### Removed
- Cleaned up users component. Removed leftover console.log.

### Added
- Improved security for users model. Denied all access to everyone and selectively allowed viewing other users only for authenticated users.
- App Settings now holds the admin's ID.

### Changed
- Gave admin ability to do everything with users
- Updated users route to /users/

## [0.0.30] - 2017-05-12
### Added
- Users component and route at /u/

### Changed
- Added link the users route to the navigation
- Updated API to allow all authenticated users to see a list of other users

## [0.0.30] - 2017-05-12
### Added
- NGINX Conf file

## [0.0.29] - 2017-05-12
### Changed
- Updated Settings Component so that users can delete their account, change their password, and update their information.
- Updated and cleaned up login process. More appropriate to set the currentUser in localStorage after the authentication service returned a response to the login component.
- Updated login component to get user data when user logs in and the token is provided to the system.

## [0.0.28] - 2017-05-12
### Hotfix
- Issue with logging out was fixed, seems steps used to log out were being done in the wrong order.

## [0.0.26] - 2017-05-11
### Added
- Dockerfile and .dockerignore files were added to angular-client/ and loopback-api/
- docker-compose.yml was added to project root

## [0.0.25] - 2017-05-11
### Added
- Node/Express server for angular dist

### Changed
- Corrected date on previous changelog entry
- Updated package.json to build angular dist and start new EN server

## [0.0.24] - 2017-05-11
### Changed
- Logout sequence updated to fix issue caused by refreshing while logged in.
- Login message
- Logout message

## [0.0.23] - 2017-05-10
### Added
- Navigation service

## [0.0.22] - 2017-05-10
### Added
- User service

## [0.0.21] - 2017-05-10
### Added
- Public guard

## [0.0.20] - 2017-05-10
### Added
- Private guard

## [0.0.19] - 2017-05-10
### Added
- Alert Service

## [0.0.18] - 2017-05-09
### Added
- Authentication Service

### Changed
- App Settings is no longer ignored. Doesn't really make sense to hide config info that is meant to be changed with each install

## [0.0.17] - 2017-05-09
### Added
- User model

## [0.0.16] - 2017-05-09
### Changed
- Default to false in app.settings for CAN_REGISTER
- Navigation component to show Register link only if public registration is enabled

## [0.0.15] - 2017-05-09
### Changed
- Updated user model to include first name, last name, and location.

## [0.0.14] - 2017-05-09
### Added
- Defaults for the API. Created admin user.

## [0.0.13] - 2017-05-09
### Added
- Alert Component

### Changed
- App Module was updated with alert component

## [0.0.12] - 2017-05-09
### Added
- Settings Component

### Changed
- App Routing was updated to handle settings route
- App Module was updated with settings component

## [0.0.11] - 2017-05-09
### Added
- Dashboard Component

### Changed
- App Routing was updated to handle dashboard route
- App Module was updated with dashboard component

## [0.0.10] - 2017-05-09
### Added
- Register Component

### Changed
- App Routing was updated to handle register route
- App Module was updated with register component

## [0.0.9] - 2017-05-09
### Added
- Login Component

### Changed
- App Routing was updated to handle login route
- App Module was updated with login component

## [0.0.8] - 2017-05-09
### Changed
- Provided basic styling for all current components with bootstrap

## [0.0.7] - 2017-05-09
### Changed
- styling for loading screen in index.html

## [0.0.6] - 2017-05-09
### Added
- Bootstrap 4.0.0-alpha.6
- Font Awesome 4.7.0
- jQuery 3.2.1

### Changed
- Updated .angular-cli.json to use new styles and scripts
- favicon.ico was moved to assets/icons
- styles.scss was moved to styles/ and renamed to override.scss
- Update index.html to use new path to favicon

## [0.0.5] - 2017-05-09
### Added
- App Routing
- Home Component

### Changed
- App Module imports App Routing and Home Component

## [0.0.4] - 2017-05-09
### Changed
- Page title in browser is now set with the title service in the app.component

## [0.0.3] - 2017-05-09
### Changed
- App navigation dynamically gets domain information from app.component now, which is set by app.settings

## [0.0.2] - 2017-05-08
### Added
- Navigation Component
- Barrels
- App Settings (Ignored by GIT)
- App Utilities

## [0.0.1] - 2017-05-08
### Added
- CHANGELOG.md
- LICENSE
- loopback-api via the lb cli tool
- angular-client via the ng cli tool

