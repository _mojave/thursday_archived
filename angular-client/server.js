'use strict';
//  ------ Require Dependencies
const express = require('express');
const path = require('path');
const http = require('http');

// ------ Configure the application
const config = require('./server_resources/config');

const app = express();

app.set('port', config.port);

app.use(express.static(path.join(__dirname, 'dist')));

// ------ Build the routes
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// ------ Serve the public
const server = http.createServer(app);
server.listen(config.port, () => console.log(`Angular Client running on http://localhost:${config.port}`));