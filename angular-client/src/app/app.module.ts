import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRouting } from './app.routing';

import { AppComponent } from './app.component';
import { NavigationComponent, AlertComponent } from './_directives/index';
import { NavigationService, AlertService, AuthenticationService, UserService } from './_services/index';
import { PublicGuard, PrivateGuard, RegistrationGuard } from './_guards/index';
import { HomeComponent, RegisterComponent, LoginComponent, DashboardComponent, SettingsComponent, UsersComponent } from './_routes/index';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    DashboardComponent,
    SettingsComponent,
    AlertComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRouting
  ],
  providers: [
    Title,
    NavigationService,
    AlertService,
    AuthenticationService,
    UserService,
    PublicGuard,
    PrivateGuard,
    RegistrationGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
