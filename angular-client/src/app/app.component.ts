import { Component, ViewChild, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { NavigationComponent } from './_directives/index';

import { Settings } from './app.settings';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public domain:any = Settings.DOMAIN;

  @ViewChild( NavigationComponent ) childView:NavigationComponent;

  constructor(
    private titleService:Title
  ) { 
    this.setTitle( this.domain.NAME );
  }

  public ngOnInit() {
  }

  public setTitle( title:string ) {
		this.titleService.setTitle( title );
	}

}
