import { Routes, RouterModule } from '@angular/router';

import { PublicGuard, PrivateGuard, RegistrationGuard } from './_guards/index';
import { HomeComponent, RegisterComponent, LoginComponent, DashboardComponent, SettingsComponent, UsersComponent } from './_routes/index';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, canActivate: [PublicGuard] },

  { path: 'register', component: RegisterComponent, canActivate: [PublicGuard, RegistrationGuard] },
  { path: 'login', component: LoginComponent, canActivate: [PublicGuard] },

  { path: 'dashboard', component: DashboardComponent, canActivate: [PrivateGuard] },
  { path: 'settings', component: SettingsComponent, canActivate: [PrivateGuard] },

  { path: 'users', component: UsersComponent, canActivate: [PrivateGuard] },
  
  { path: '**', redirectTo: '' }
];

export const AppRouting = RouterModule.forRoot(appRoutes);
