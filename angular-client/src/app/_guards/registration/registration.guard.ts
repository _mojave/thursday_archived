import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Settings } from '../../app.settings';

@Injectable()
export class RegistrationGuard implements CanActivate {

  constructor(
      private router:Router
  ) { }

  public canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
    if (Settings.DOMAIN.CAN_REGISTER) {
        // Public registration is enabled so return true
        return true;
    }
    // Public registration is disabled so redirect to home page with the return url so the user can figure out what to do
    this.router.navigate(['/'], { queryParams: { returnUrl: state.url }});
    return false;
  }

}
