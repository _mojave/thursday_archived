import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { NavigationService } from '../../_services/index';

@Injectable()
export class PublicGuard implements CanActivate {

    constructor(
        private router:Router,
        private navigationService:NavigationService
    ) { }

    public canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            this.router.navigate(['/dashboard']);
            return false;
        }
        this.navigationService.setNavigationOptions({isLoggedIn: false});
        return true;
    }

}
