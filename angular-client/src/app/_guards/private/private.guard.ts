import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { NavigationService } from '../../_services/index';

@Injectable()
export class PrivateGuard implements CanActivate {

    constructor(
        private router:Router,
        private navigationService:NavigationService
    ) { }

    public canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            this.navigationService.setNavigationOptions({isLoggedIn: true});
            return true;
        }
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }

}
