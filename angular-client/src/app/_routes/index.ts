export * from './home/home.component';

export * from './register/register.component';
export * from './login/login.component';

export * from './dashboard/dashboard.component';
export * from './settings/settings.component';

export * from './users/users.component';