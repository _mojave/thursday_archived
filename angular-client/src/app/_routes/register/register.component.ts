import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, UserService } from '../../_services/index';

import { Settings } from '../../app.settings';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    public domain:boolean = Settings.DOMAIN;

    public pageTitle:string = 'Register';

    public model:any = {};
    public loading:boolean = false;

    constructor(
        private router:Router,
        private userService:UserService,
        private alertService:AlertService
    ) { }

    public ngOnInit() {
    }
    
    public register() {
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
