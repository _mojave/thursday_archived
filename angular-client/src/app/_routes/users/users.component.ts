import { Component, OnInit } from '@angular/core';

import { User } from '../../_models/index';
import { UserService } from '../../_services/index';

import { Settings } from '../../app.settings';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  public pageTitle:string = 'Users';

  public currentUser:User;
  public users:User[] = [];

  public domain:any = Settings.DOMAIN;

  constructor(
    private userService:UserService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  public ngOnInit() {
    this.loadAllUsers();
  }

  private loadAllUsers() {
    this.userService
      .getAll()
      .subscribe(users => { 
        this.users = users;
      });
  }

  public deleteUser(id: number) {
    this.userService
      .delete(id)
      .subscribe(() => {
        this.loadAllUsers();
      });
  }

}
