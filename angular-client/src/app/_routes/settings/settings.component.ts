import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../_models/index';
import { AlertService, UserService, AuthenticationService } from '../../_services/index';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  public pageTitle:string = 'Settings';

  public currentUser:User;
  public model:any = {};
  public loading:boolean = false;

  constructor(
    private router:Router,
    private alertService:AlertService,
    private userService:UserService,
    private authenticationService:AuthenticationService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  public ngOnInit() {
  }

  public updateOwnUser() { // TODO: This could be better
    this.loading = true;

    this.model.id = this.currentUser.id;
    this.model.firstName = this.model.firstName || this.currentUser.firstName;
    this.model.lastName = this.model.lastName || this.currentUser.lastName;
    this.model.username = this.model.username || this.currentUser.username;
    this.model.email = this.model.email || this.currentUser.email;
    this.model.location = this.model.location || this.currentUser.location;
    this.model.bio = this.model.bio || this.currentUser.bio;

    this.userService.update(this.model)
      .subscribe(
        user => {
          this.alertService.success('Your information was successfully updated.', true);
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.router.navigate(['/dashboard']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  public changeOwnPassword() {
    this.loading = true;
    this.userService.changePassword(this.model.oldPassword, this.model.newPassword)
      .subscribe(
        data => {
          this.alertService.success('Your password was successfully changed.', true);
          this.router.navigate(['/dashboard']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  public deleteOwnUser() {
    if (this.model.usernameVerifier === this.currentUser.username) {
      this.loading = true;
      this.userService
        .delete(this.currentUser.id)
        .subscribe(
          data => {
            this.alertService.success('Your account has been deleted.', true);
            this.authenticationService.logout();
            localStorage.removeItem('currentUser');
            localStorage.removeItem('userToken');
            this.router.navigate(['/login']);
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          });
    } else {
      this.alertService.error('Oops! That\'s not your username.', false);
    }
  }

}
