import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthenticationService, UserService, AlertService } from '../../_services/index';

import { Settings } from '../../app.settings';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public canRegister:boolean = Settings.DOMAIN.CAN_REGISTER;

  public pageTitle:string = 'Login';

  public model:any = {};
  public loading:boolean = false;
  public returnUrl:string;

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private authenticationService:AuthenticationService,
    private userService:UserService,
    private alertService:AlertService
  ) { }

  public ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
  }

  public login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(
        token => {
          this.alertService.success('You\'ve successfully logged in.', true);
          if (token && token.id) {
            localStorage.setItem('userToken', JSON.stringify(token));
            this.getOwnUser(token.userId)
          }
          
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  public getOwnUser(id:number) {
    this.userService
      .getById(id)
      .subscribe(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.router.navigate([this.returnUrl]);
      });
  }

}
