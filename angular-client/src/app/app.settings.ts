export class Settings {

  public static DOMAIN:any = {
    ADMIN_ID: 1,
    NAME: 'Thursday',
    CAN_REGISTER: true,
    API_BASE_URL: 'http://' + window.location.hostname + ':3000/api'
  };

}
