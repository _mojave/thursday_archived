import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../../_models/index';
import { NavigationService, AlertService, AuthenticationService } from '../../_services/index';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  public currentUser:User;
  public isLoggedIn:boolean;

  public returnUrl:string;

  @Input() domain;

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private alertService:AlertService,
    private navigationService:NavigationService,
    private authenticationService:AuthenticationService
  ) { }

  public ngOnInit() {
    this.getAuthenticationStatus();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';
  }

  public getAuthenticationStatus() {
    this.navigationService
      .getNavigationOptions()
      .subscribe(options => { 
        this.isLoggedIn = options.isLoggedIn;
      });
  }

  public logout() {
    this.authenticationService
      .logout()
      .subscribe(
        data => {
          this.alertService.success('You\'ve successfully logged out.', true);
          localStorage.removeItem('currentUser');
          localStorage.removeItem('userToken');
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.alertService.error(error);
        });
  }

}
