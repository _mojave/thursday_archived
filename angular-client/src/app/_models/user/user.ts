export class User {
    token:any;
    id:number;
    email:string;
    username:string;
    password:string;
    firstName:string;
    lastName:string;
    location:string;
    bio:string;
}
