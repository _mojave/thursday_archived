import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { Settings } from '../../app.settings';

@Injectable()
export class AuthenticationService {

  constructor(
      private http:Http
  ) { }

  public login(username:string, password:string) {
    return this.http
      .post(`${Settings.DOMAIN.API_BASE_URL}/users/login`, { username: username, password: password })
      .map((response:Response) => response.json());
  }

  public logout() {
    return this.http
      .post(`${Settings.DOMAIN.API_BASE_URL}/users/logout`, { }, this.tokenizeHeaders())
      .map((response:Response) => response.json());
  }

// private helper methods

  private tokenizeHeaders() {
    // create authorization header with jwt token
    let userToken = JSON.parse(localStorage.getItem('userToken'));
    if (userToken && userToken.id) {
      let headers = new Headers({
        "Content-Type": "application/json",
        'Authorization': userToken.id
      });
      return new RequestOptions({ headers: headers });
    }
  }

}
