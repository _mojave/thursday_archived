import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User } from '../../_models/index';

import { Settings } from '../../app.settings';

@Injectable()
export class UserService {

  constructor(
    private http:Http
  ) { }

  public getAll() {
    return this.http
      .get(`${Settings.DOMAIN.API_BASE_URL}/users`, this.tokenizeHeaders())
      .map((response:Response) => response.json());
  }

  public getById(id:number) {
    return this.http
      .get(`${Settings.DOMAIN.API_BASE_URL}/users/` + id, this.tokenizeHeaders())
      .map((response:Response) => response.json());
  }

  public create(user:User) {
    return this.http
      .post(`${Settings.DOMAIN.API_BASE_URL}/users`, user, this.tokenizeHeaders())
      .map((response:Response) => response.json());
  }

  public update(user:User) {
    return this.http
      .put(`${Settings.DOMAIN.API_BASE_URL}/users/` + user.id, user, this.tokenizeHeaders())
      .map((response:Response) => response.json());
  }

  public changePassword(oldPassword:string, newPassword:string) {
    return this.http
      .post(`${Settings.DOMAIN.API_BASE_URL}/users/change-password`, { oldPassword: oldPassword, newPassword: newPassword }, this.tokenizeHeaders())
      .map((response:Response) => response.json());
  }

  public delete(id:number) {
    return this.http
      .delete(`${Settings.DOMAIN.API_BASE_URL}/users/` + id, this.tokenizeHeaders())
      .map((response:Response) => response.json());
  }

  // private helper methods

  private tokenizeHeaders() {
    // create authorization header with tokenizeHeaders token
    let userToken = JSON.parse(localStorage.getItem('userToken'));
    if (userToken && userToken.id) {
      let headers = new Headers({
        "Content-Type": "application/json",
        'Authorization': userToken.id
      });
      return new RequestOptions({ headers: headers });
    }
  }

}