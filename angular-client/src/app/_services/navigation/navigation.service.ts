import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class NavigationService {

  private subject = new Subject<any>();
  private isLoggedIn:boolean = false;

  constructor() { }

  public setNavigationOptions(options:any) {
      this.subject.next({ isLoggedIn: options.isLoggedIn });
  }

  public getNavigationOptions(): Observable<any> {
      return this.subject.asObservable();
  }

}
