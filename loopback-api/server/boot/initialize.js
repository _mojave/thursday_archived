module.exports = function(app) {
	var User = app.models.user;
	var Role = app.models.Role;
	var RoleMapping = app.models.RoleMapping;

	User.create({
		username: 'admin', 
		email: 'admin@thurs.day', 
		password: 'Thursday'
	}, function(err, user) {
		if (err) throw err;
		console.log('Created user:', user);

		// ------ create the admin role
		Role.create({
			name: 'admin'
		}, function(err, role) {
			if (err) throw err;
			console.log('Created role:', role);

			// ------ make the first use the admin
			role.principals.create({
				principalType: RoleMapping.USER,
				principalId: user.id
			}, function(err, principal) {
				if (err) throw err;
				console.log('Created principal:', principal);
			});
		});
	});
};